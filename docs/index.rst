.. HelpDev documentation master file, created by
   sphinx-quickstart on Tue May 14 17:04:31 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to HelpDev's documentation!
===================================

.. toctree::
   :maxdepth: 2

   readme.rst
   reference/helpdev.rst
   tutorials/index.rst
   changes.rst

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
