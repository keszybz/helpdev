Personalising for your application
----------------------------------

You can use this tool to provide a easy way to the users get necessary
information about their environment when reporting bugs. Even if developers
can use it to *easilly* get all the information necessary.

1. Add ``helpdev`` to your list of requirements in ``setup.py``

.. code-block:: Python

    # ...

    install_requires=['helpdev'...]

    # ...


2. Import ``helpdev`` functions that are important for you

.. code-block:: Python

    # ...

    import helpdev

    report_dict = {}

    # get basic information updating the dictionary
    report_dict.update(helpdev.check_hardware())
    report_dict.update(helpdev.check_os())
    report_dict.update(helpdev.check_python())
    report_dict.update(helpdev.check_qt_bindings())
    report_dict.update(helpdev.check_qt_abstractions())

    # list of important packages for your app
    # note that you can use regex (spyder.*)
    packages = "spyder.*,ipython,cython,jedi,matplotlib,numpy,pandas,"
               "psutil,pycodestyle,pyflakes,pygments,pylint,qtconsole,"
               "rope,sphinx,sympy"

    # get filtered information for those packages
    report_dict.update(helpdev.check_python_packages(packages))

    # ...


3. You can use, then, the dictionary-like information or print it

.. code-block:: Python

    # ...

    # printing the output in the terminal
    helpdev.print_output(report_dict)

    # ...

4. The output for this example is, then


.. code-block:: console

    * HARDWARE-----------------------------------------------------------------------
        - Machine....................... x86_64
        - Processor..................... Intel(R) Core(TM) i7-4790K CPU @ 4.00GHz
        - Total Memory.................. 16689 MB
        - Free Memory................... 534 MB
        - Total Swap.................... 19999 MB
        - Free Swap..................... 19999 MB
    * OPERATING SYSTEM---------------------------------------------------------------
        - System........................ Linux
        - Release....................... 4.15.0-48-generic
        - Platform...................... Linux-4.15.0-48-generic-x86_64-with-debian-buster-sid
        - Version....................... #51-Ubuntu SMP Wed Apr 3 08:28:49 UTC 2019
    * PYTHON DISTRIBUTION------------------------------------------------------------
        - Version....................... 3.6.8
        - C Compiler.................... GCC 7.3.0
        - C API Version................. 1013
        - Implementation................ cpython
        - Implementation Version........ 3.6.8
    * QT BINDINGS--------------------------------------------------------------------
        - PyQt5 Version................. 5.12.2
        - PyQt5 Qt Version.............. 5.12.3
    * QT ABSTRACTIONS----------------------------------------------------------------
        - qtpy Version.................. 1.7.1
        - qtpy Binding.................. pyqt5
        - qtpy Binding Variable......... os.environ['QT_API']
        - qtpy Import Name.............. qtpy
        - qtpy Status................... OK
        - pyqtgraph Version............. 0.10.0
        - pyqtgraph Binding............. Not set or inexistent
        - pyqtgraph Binding Variable.... os.environ['PYQTGRAPH_QT_LIB']
        - pyqtgraph Import Name......... pyqtgraph
        - pyqtgraph Status.............. OK
        - Qt Version.................... 1.1.0
        - Qt Binding.................... PyQt5
        - Qt Binding Variable........... Qt.__binding__
        - Qt Import Name................ Qt
        - Qt Status..................... OK
    * PYTHON PACKAGES----------------------------------------------------------------
        - ipython....................... 7.5.0
        - jedi.......................... 0.13.3
        - numpy......................... 1.16.3
        - psutil........................ 5.6.2
        - pycodestyle................... 2.5.0
        - pyflakes...................... 2.1.1
        - Pygments...................... 2.3.1
        - qtconsole..................... 4.4.4
        - rope.......................... 0.14.0
        - Sphinx........................ 2.0.1
        - spyder........................ 3.3.4
        - spyder-kernels................ 0.4.4


You can obtain the same output using the command-line options as follows

.. code-block:: console

    helpdev --hardware --os --python --qt

    helpdev --packages="spyder.*,ipython,cython,jedi,matplotlib,numpy,
                        pandas,psutil,pycodestyle,pyflakes,pygments,
                        pylint,qtconsole,rope,sphinx,sympy"


In this example, the packages list was created from the Spyder dependencies
list and *about* which can be seem below:


.. image:: ../images/spyder-dep-list.png


.. image:: ../images/spyder-about.png
    :width: 400
